"use strict";
function convertCToF(celsius) {
    let cTemp = celsius;
    let cToFahr = cTemp * 9 / 5 + 32;
    let message = cTemp + '\xB0C is ' + cToFahr + '\xB0F.';
    console.log(message);
}
function convertFtoC(fahrenheit) {
    let fTemp = fahrenheit;
    let fToCel = (fTemp - 32) * 5 / 9;
    let message = fTemp + '\xB0F is ' + fToCel + '\xB0C.';
    console.log(message);
}
const currentTemp = [212, 90, 72, 32, 0, -40];
let i = 0;
while (i < currentTemp.length) {
    convertFtoC(currentTemp[i]);
    convertCToF(currentTemp[i]);
    i++;
}
// for (;currentTemp[i];) {
//   console.log(i);
//   convertFtoC(currentTemp[i]);
//   convertCToF(currentTemp[i]);
//   i++;
// }
