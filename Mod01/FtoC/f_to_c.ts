function convertCToF(celsius: number) {
    let cTemp: number = celsius;
    let cToFahr: number = cTemp * 9 / 5 + 32;
    let message: string = cTemp + '\xB0C is ' + cToFahr + '\xB0F.';
    console.log(message);
}

function convertFtoC(fahrenheit: number) {
    let fTemp: number = fahrenheit;
    let fToCel: number = (fTemp - 32) * 5 / 9;
    let message: string = fTemp + '\xB0F is ' + fToCel + '\xB0C.';
    console.log(message);
}

const currentTemp: number[] = [212, 90, 72, 32, 0, -40];
let i: number = 0;

while (i < currentTemp.length) {
    convertFtoC(currentTemp[i]);
    convertCToF(currentTemp[i]);
    i++;
}

// for (;currentTemp[i];) {
//   console.log(i);
//   convertFtoC(currentTemp[i]);
//   convertCToF(currentTemp[i]);
//   i++;
// }
