"use strict";
let payRate = 10;
let hoursWorked = 45;
let filingStatus = "Single";
function weeklyGrossPay(payRate, hoursWorked) {
    let hoursOver40 = hoursWorked - 40;
    if (hoursOver40 < 0) {
        hoursOver40 = 0;
    }
    else {
        hoursWorked = 40;
    }
    return (payRate * hoursWorked) + (payRate * 1.5 * hoursOver40);
}
let estAnnualGrossPay = weeklyGrossPay(payRate, hoursWorked) * 52;
let taxRate = 0;
if (filingStatus == "Single") {
    if (estAnnualGrossPay < 23000) {
        taxRate = .05;
    }
    else if (estAnnualGrossPay >= 23000 && estAnnualGrossPay <= 74999.99) {
        taxRate = .12;
    }
    else {
        taxRate = .20;
    }
}
else {
    if (estAnnualGrossPay < 23000) {
        taxRate = 0;
    }
    else if (estAnnualGrossPay >= 23000 && estAnnualGrossPay <= 74999.99) {
        taxRate = .09;
    }
    else {
        taxRate = .20;
    }
}
let taxWithholdings = estAnnualGrossPay * taxRate;
let netPay = estAnnualGrossPay - taxWithholdings;
console.log(`You worked ${hoursWorked} hours this period`);
console.log(`Because you earn $${payRate} per hour, your annual gross pay is $${estAnnualGrossPay}`);
console.log(`Your filing status is ${filingStatus}`);
console.log(`Your annual tax withholdings is $${taxWithholdings}`);
console.log(`Your net pay is $${netPay}`);
