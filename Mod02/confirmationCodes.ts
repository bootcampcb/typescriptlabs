function getProduct(code: string): string {
    let myProduct: string = code.substr(0, code.indexOf("-"));
    return (myProduct[0] + myProduct.slice(1).toLowerCase());
}

function getSize(code: string) {
    let size: string = code.substring(code.indexOf("-") + 1, code.lastIndexOf("-"));
    let sizeString: string = "";

    if (size == "S") {
        sizeString = "Small";
    } else if (size == "M") {
        sizeString = "Medium";
    } else if (size == "L") {
        sizeString = "Large";
    }
    else if (size == "XL") {
        sizeString = "Extra large";
    }
    return sizeString
}

function getDate(code: string) {
    let pickDate: string = code.substr(code.lastIndexOf("-") + 1);
    pickDate = pickDate.substring(0,2) + "-" + pickDate.substring(2,4);
    return pickDate;
}

const productCode: string[] = ["HAM-L-1220", "TURKEY-M-1125"];
let i: number = 0;

while (i < productCode.length) {
    console.log(`${getSize(productCode[i])} ${getProduct(productCode[i])} picked up on ${getDate(productCode[i])}`);
    i++;
}