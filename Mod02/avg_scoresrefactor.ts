let scores: Array<number> = [
    93, 86, 73, 79, 83, 100, 94
];

function sumNumbersInArray(scores: Array<number>): number {
    let numScores = scores.length;
    let sumScores = 0;

    for (let i = 0; i < numScores; i++) {
        sumScores += scores[i];
    }
    return sumScores;
}

function getHighScore(scores: Array<number>): number {
    return(scores[scores.length - 1]);
}
function getLowScore(scores: Array<number>): number {
    return(scores[0]);
}
function displayStat(message: string, num: number): void {
    console.log(`${message}: ${num}`);
}

let sum: number = sumNumbersInArray(scores);
let average: number = sum / scores.length;;
scores.sort(function (a, b) {
    return a - b;
});
let highScore: number = getHighScore(scores);
let lowScore: number = getLowScore(scores);
// The displayStat function will concatenate the two 
// values passed separated by a color and display 
// the result
displayStat("Average", average);
displayStat("High Score", highScore);