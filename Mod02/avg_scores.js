"use strict";
let myScores = [92, 98, 84, 76, 89, 99, 100]; /* Odd */
let yourScores = [72, 82, 98, 94, 88, 92, 100, 100]; /* Even */
function getAverage(scores) {
    let numScores = scores.length;
    let sumScores = 0;
    let countScores = 0;
    for (let i = 0; i < numScores; i++) {
        sumScores += scores[i];
        countScores++;
    }
    return (sumScores / countScores);
}
function getMedian(scores) {
    scores.sort(function (a, b) {
        return a - b;
    });
    console.log(scores);
    let modulo = scores.length % 2;
    if (modulo == 0) {
        let pos = Math.trunc(scores.length / 2);
        let avg = (scores[pos - 1] + scores[pos]) / 2;
        return avg;
    }
    else {
        let pos = Math.trunc(scores.length / 2);
        return scores[pos];
    }
}
console.log("My avg scores:", getAverage(myScores));
console.log("My median scores - odd:", getMedian(myScores));
console.log("My low score:", myScores[0]);
console.log("My high score:", myScores[myScores.length - 1]);
console.log("Your avg scores:", getAverage(yourScores));
console.log("Your median scores - even:", getMedian(yourScores));
console.log("Your low score:", yourScores[0]);
console.log("Your high score:", yourScores[yourScores.length - 1]);
