"use strict";
let scores = [
    93, 86, 73, 79, 83, 100, 94
];
function sumNumbersInArray(scores) {
    let numScores = scores.length;
    let sumScores = 0;
    for (let i = 0; i < numScores; i++) {
        sumScores += scores[i];
    }
    return sumScores;
}
function getHighScore(scores) {
    return (scores[scores.length - 1]);
}
function getLowScore(scores) {
    return (scores[0]);
}
function displayStat(message, num) {
    console.log(`${message}: ${num}`);
}
let sum = sumNumbersInArray(scores);
let average = sum / scores.length;
;
scores.sort(function (a, b) {
    return a - b;
});
let highScore = getHighScore(scores);
let lowScore = getLowScore(scores);
// The displayStat function will concatenate the two 
// values passed separated by a color and display 
// the result
displayStat("Average", average);
displayStat("High Score", highScore);
