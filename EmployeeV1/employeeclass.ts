class Employee {
    private stuid: number;
    public name: string;
    public jobTitle: string;
    public payRate: number;

    constructor(stuid: number, name: string, jobTitle: string, payRate: number) {
        this.stuid = stuid;
        this.name = name;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }
    toString(): string {
        return `${this.name} (${this.stuid}) is a ${this.jobTitle} earning $${this.payRate.toFixed(2)}/hr`;
    }
}
let emp1 = new Employee(1, "Ezra Aiden", "Truck Driver", 35.00);
console.log( emp1.toString() );
let emp2 = new Employee(2, "Zephaniah Redd", "Bus Driver", 25.00);
console.log( emp2.toString() );